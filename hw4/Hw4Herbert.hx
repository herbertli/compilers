module compilers.Hw4Herbert {

    // Special characters for HACS (so I can copy and paste them): ⟨, ⟩, ¬, ¶, ↑, →, ↓, ⟦, ⟧)

    space [ \t\n] ;
    token Elem | [0-9]+ ;

    sort List | ⟦ ⟨Elem⟩ ⟨List⟩ ⟧ | ⟦ { ⟨List⟩ } ⟨List⟩ ⟧ | ⟦⟧ ;

    // Product
    sort Computed | scheme Product(List) | scheme ProductH(List, Computed);
    Product(#) → ProductH(#, ⟦ 1 ⟧) ;
    ProductH(⟦ ⟧, #n) → #n ;
    ProductH(⟦ ⟨Elem#1⟩ ⟨List#2⟩ ⟧, #n) → ProductH(#2, ⟦ #1 * #n ⟧) ;
    ProductH(⟦ { ⟨List#1⟩ } ⟨List#2⟩ ⟧, #n) → ProductH(#1, ProductH(#2, #n)) ;

    // Flatten - (not completely implemented)
    sort List | scheme Flatten(List);
    Flatten(⟦ ⟧) → ⟦ ⟧ ;
    Flatten(⟦ ⟨Elem#1⟩ ⟨List#2⟩ ⟧) → ⟦ ⟨Elem#1⟩ ⟨List Flatten(#2)⟩ ⟧ ;
    
    // This is what I want, but the program does not compile...
    // I think it's because ⟨List⟩ ⟨List⟩ is not a valid sort
    // Flatten(⟦ { ⟨List#1⟩ } ⟨List#2⟩ ⟧) → ⟦ ⟨List Flatten(#1)⟩ ⟨List Flatten(#2)⟩ ⟧ ;

    // Reverse
    sort List | scheme Reverse(List) | scheme ReverseH(List, List);
    Reverse(#) → ReverseH(#, ⟦ ⟧) ;
    ReverseH(⟦ ⟧, #1) → #1 ;
    ReverseH(⟦ ⟨Elem#1⟩ ⟨List#2⟩ ⟧, #3) → ReverseH(#2, ⟦ ⟨Elem#1⟩ ⟨List#3⟩ ⟧);
    ReverseH(⟦ { ⟨List#1⟩ } ⟨List#2⟩ ⟧, #3) → ReverseH(#2, ⟦ { ⟨List Reverse(#1)⟩ } ⟨List#3⟩ ⟧) ;

}
