module compilers.Hw6Herbert {

    // ============================
    // Tokens and Grammar
    // ============================
    space [ \t\n] ;
    token Id | [a-zA-Z]+ ;

    main sort BProg | ⟦ ⟨BBlock⟩ ⟧ ;
    sort BBlock | ⟦ { ⟨BStat⟩ } ⟧ ;
    sort BStat
        | ⟦ def ⟨Id⟩; ⟨BStat⟩ ⟧
        | ⟦ use ⟨Id⟩; ⟨BStat⟩ ⟧
        | ⟦ ⟨BBlock⟩ ⟨BStat⟩ ⟧
        | ⟦ ⟧ ;

    sort Bool | ⟦ true ⟧ | ⟦ false ⟧ ;
    sort Bool | scheme And(Bool, Bool) ;
    And(⟦ true ⟧, #) → # ;
    And(⟦ false ⟧, #) → ⟦ false ⟧ ;

    // ============================
    // attribute for ok
    // ============================
    attribute ↑ok(Bool) ;

    // ============================
    // ok carriers
    // ============================
    sort BProg | ↑ok ;
    ⟦ ⟨BBlock#1 ↑ok(#ok)⟩ ⟧↑ok(#ok) ;
    sort BBlock | ↑ok ;
    ⟦ { ⟨BStat#1 ↑ok(#ok)⟩ } ⟧↑ok(#ok) ;
    sort BStat | ↑ok ;
    ⟦ def ⟨Id#1⟩; ⟨BStat#2 ↑ok(#ok)⟩ ⟧↑ok(#ok) ;
    // use will be handled by e-carriers below
    ⟦ ⟨BBlock#1 ↑ok(#ok)⟩ ⟨BStat#2 ↑ok(#ok2)⟩ ⟧↑ok(And(#ok, #ok2)) ;
    ⟦ ⟧↑ok(⟦ true ⟧) ;

    // ============================
    // attribute for environment - Value will always be "true"
    // ============================
    attribute ↓e{Id : Bool} ;

    // ============================
    // e-carriers:
    // ============================
    sort BProg | scheme BProgE(BProg) ↓e;
    BProgE(⟦ ⟨BBlock#1⟩ ⟧) → ⟦ ⟨BBlock BBlockE(#1)⟩ ⟧ ;

    sort BBlock | scheme BBlockE(BBlock) ↓e;
    BBlockE(⟦ { ⟨BStat#1⟩ } ⟧) → ⟦ { ⟨BStat BStatE(#1)⟩ } ⟧ ;

    sort BStat | scheme BStatE(BStat) ↓e;
    BStatE(⟦ def ⟨Id#1⟩; ⟨BStat#2⟩ ⟧) → ⟦ def ⟨Id#1⟩; ⟨BStat BStatE(#2) ↓e{#1 : true}⟩ ⟧ ;
    BStatE(⟦ use ⟨Id#1⟩ ; ⟨BStat#2 ↑ok(#ok2)⟩ ⟧) ↓e{#1 : #t} → ⟦ use ⟨Id#1⟩ ; ⟨BStat#2⟩ ⟧↑ok(#ok2) ;
    BStatE(⟦ use ⟨Id#1⟩ ; ⟨BStat#2 ↑ok(#ok2)⟩ ⟧) ↓e{¬#1} → ⟦ use ⟨Id#1⟩ ; ⟨BStat#2⟩ ⟧↑ok(⟦ false ⟧) ;
    BStatE(⟦ ⟨BBlock#1⟩ ⟨BStat#2⟩ ⟧) → ⟦ ⟨BBlock BBlockE(#1)⟩ ⟨BStat BStatE(#2)⟩ ⟧ ;
    BStatE(⟦ ⟧↑#syn) → ⟦ ⟧↑#syn ;

    // bootstrap since ok depends on e
    sort Bool | scheme IsOk(BProg) | scheme IsOk2(BProg) ;
    IsOk(#) → IsOk2(BProgE(#)) ;
    IsOk2(# ↑ok(#ok)) →#ok ;

}
