module edu.nyu.cs.cc.Pr3Base {

  /*
    TODO:
    Address of
    save function argument variables
    Spill  :(
  */


  ////////////////////////////////////////////////////////////////////////
  // 1. MiniC LEXICAL ANALYSIS AND GRAMMAR
  ////////////////////////////////////////////////////////////////////////

  // pr1: refers to http://cs.nyu.edu/courses/fall16/CSCI-GA.2130-001/project1/pr1.pdf

  // TOKENS (pr1:1.1).

  space [ \t\n\r] | '//' [^\n]* | '/*' ( [^*] | '*' [^/] )* '*/'  ; // Inner /* ignored

  token ID      | ⟨LetterEtc⟩ (⟨LetterEtc⟩ | ⟨Digit⟩)* ;
  token INT | ⟨Digit⟩+ ;
  token STR | "\"" ( [^\"\\\n] | \\ ⟨Escape⟩ )* "\"";

  token fragment Letter     | [A-Za-z] ;
  token fragment LetterEtc  | ⟨Letter⟩ | [$_] ;
  token fragment Digit      | [0-9] ;

  token fragment Escape  | [\n\\nt"] | "x" ⟨Hex⟩ ⟨Hex⟩ | ⟨Octal⟩;
  token fragment Hex     | [0-9A-Fa-f] ;
  token fragment Octal   | [0-7] | [0-7][0-7] | [0-7][0-7][0-7];

  // PROGRAM (pr1:2.6)

  main sort Program  |  ⟦ ⟨Declarations⟩ ⟧ ;

  // DECLARATIONS (pr1:1.5)

  sort Declarations | ⟦ ⟨Declaration⟩ ⟨Declarations⟩ ⟧ | ⟦⟧ ;

  sort Declaration
    |  ⟦ function ⟨Type⟩ ⟨Identifier⟩ ⟨ArgumentSignature⟩ { ⟨Statements⟩ } ⟧
    ;

  sort ArgumentSignature
    |  ⟦ ( ) ⟧
    |  ⟦ ( ⟨Type⟩ ⟨Identifier⟩ ⟨TypeIdentifierTail⟩ ) ⟧
    ;
  sort TypeIdentifierTail |  ⟦ , ⟨Type⟩ ⟨Identifier⟩ ⟨TypeIdentifierTail⟩ ⟧  |  ⟦ ⟧ ;

  // STATEMENTS (pr1:1.4)

  sort Statements
    | ⟦ { ⟨Statements⟩ } ⟨Statements⟩ ⟧
    | ⟦ var ⟨Type⟩ ⟨Identifier⟩ ; ⟨Statements⟩ ⟧
    | ⟦ ⟨Expression⟩ = ⟨Expression⟩ ; ⟨Statements⟩ ⟧
    | ⟦ if ( ⟨Expression⟩ ) {⟨Statements⟩} else {⟨Statements⟩} ⟨Statements⟩ ⟧
    | ⟦ if ( ⟨Expression⟩ ) {⟨Statements⟩} ⟨Statements⟩ ⟧
    | ⟦ while ( ⟨Expression⟩ ) {⟨Statements⟩} ⟨Statements⟩ ⟧
    | ⟦ return ⟨Expression⟩ ; ⟨Statements⟩ ⟧
    | ⟦⟧
    ;

  // TYPES (pr1:1.3)

  sort Type
    |  ⟦ int ⟧@3
    |  ⟦ char ⟧@3
    |  ⟦ ( ⟨Type⟩ )⟧@3
    |  ⟦ ⟨Type@2⟩ ( ⟨TypeList⟩ )⟧@2
    |  ⟦ * ⟨Type@1⟩ ⟧@1
    ;

  sort TypeList | ⟦ ⟨Type⟩ ⟨TypeListTail⟩ ⟧ | ⟦⟧;
  sort TypeListTail | ⟦ , ⟨Type⟩ ⟨TypeListTail⟩ ⟧ | ⟦⟧;

  // EXPRESSIONS (pr1:2.2)

  sort Expression

    |  sugar ⟦ ( ⟨Expression#e⟩ ) ⟧@10 → #e

    |  ⟦ ⟨Integer⟩ ⟧@10
    |  ⟦ ⟨String⟩ ⟧@10
    |  ⟦ ⟨Identifier⟩ ⟧@10

    |  ⟦ ⟨Expression@9⟩ ( ⟨ExpressionList⟩ ) ⟧@9
    |  ⟦ null ( ⟨Type⟩ ) ⟧@9
    |  ⟦ sizeof ( ⟨Type⟩ )⟧@9

    |  ⟦ ! ⟨Expression@8⟩ ⟧@8
    |  ⟦ - ⟨Expression@8⟩ ⟧@8
    |  ⟦ + ⟨Expression@8⟩ ⟧@8
    |  ⟦ * ⟨Expression@8⟩ ⟧@8
    |  ⟦ & ⟨Expression@8⟩ ⟧@8

    |  ⟦ ⟨Expression@7⟩ * ⟨Expression@8⟩ ⟧@7

    |  ⟦ ⟨Expression@6⟩ + ⟨Expression@7⟩ ⟧@6
    |  ⟦ ⟨Expression@6⟩ - ⟨Expression@7⟩ ⟧@6

    |  ⟦ ⟨Expression@6⟩ < ⟨Expression@6⟩ ⟧@5
    |  ⟦ ⟨Expression@6⟩ > ⟨Expression@6⟩ ⟧@5
    |  ⟦ ⟨Expression@6⟩ <= ⟨Expression@6⟩ ⟧@5
    |  ⟦ ⟨Expression@6⟩ >= ⟨Expression@6⟩ ⟧@5

    |  ⟦ ⟨Expression@5⟩ == ⟨Expression@5⟩ ⟧@4
    |  ⟦ ⟨Expression@5⟩ != ⟨Expression@5⟩ ⟧@4

    |  ⟦ ⟨Expression@3⟩ && ⟨Expression@4⟩ ⟧@3

    |  ⟦ ⟨Expression@2⟩ || ⟨Expression@3⟩ ⟧@2
    ;

  // Helper to describe actual list of arguments of function call.
  sort ExpressionList | ⟦ ⟨Expression⟩ ⟨ExpressionListTail⟩ ⟧  |  ⟦⟧ ;
  sort ExpressionListTail | ⟦ , ⟨Expression⟩ ⟨ExpressionListTail⟩ ⟧  |  ⟦⟧ ;

  sort Integer      | ⟦ ⟨INT⟩ ⟧ ;
  sort String       | ⟦ ⟨STR⟩ ⟧ ;
  sort Identifier   | symbol ⟦⟨ID⟩⟧ ;


  ////////////////////////////////////////////////////////////////////////
  // 2. MinARM32 ASSEMBLER GRAMMAR
  ////////////////////////////////////////////////////////////////////////

  // arm: refers to http://cs.nyu.edu/courses/fall14/CSCI-GA.2130-001/pr3/MinARM32.pdf

  // Instructions.
  sort Instructions | ⟦ ⟨Instruction⟩ ⟨Instructions⟩ ⟧ | ⟦⟧ ;

  // Directives (arm:2.1)
  sort Instruction
    | ⟦DEF ⟨Identifier⟩ = ⟨Integer⟩ ¶⟧  // define identifier TODO make sure correct
    | ⟦¶⟨Label⟩ ⟧               // define address label
    | ⟦DCI ⟨Integers⟩ ¶⟧        // allocate integers
    | ⟦DCS ⟨String⟩ ¶⟧          // allocate strings
    | ⟦⟨Op⟩ ¶⟧                  // machine instruction
    ;

  sort Integers | ⟦ ⟨Integer⟩, ⟨Integers⟩ ⟧ | ⟦ ⟨Integer⟩ ⟧ ;

  sort Label | symbol ⟦⟨ID⟩⟧ ; // TODO: changed!!!!

  // Syntax of individual machine instructions (arm:2.2).
  sort Op

    | ⟦MOV ⟨Reg⟩, ⟨Arg⟩ ⟧       // move
    | ⟦MVN ⟨Reg⟩, ⟨Arg⟩ ⟧       // move not
    | ⟦ADD ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧    // add
    | ⟦SUB ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧    // subtract
    | ⟦RSB ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧    // reverse subtract
    | ⟦AND ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧    // bitwise and
    | ⟦ORR ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧    // bitwise or
    | ⟦EOR ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧    // bitwise exclusive or
    | ⟦CMP ⟨Reg⟩, ⟨Arg⟩ ⟧           // compare
    | ⟦MUL ⟨Reg⟩, ⟨Reg⟩, ⟨Reg⟩ ⟧    // multiply

    | ⟦B ⟨Label⟩ ⟧          // branch always
    | ⟦BEQ ⟨Label⟩ ⟧            // branch if equal
    | ⟦BNE ⟨Label⟩ ⟧            // branch if not equal
    | ⟦BGT ⟨Label⟩ ⟧            // branch if greater than
    | ⟦BLT ⟨Label⟩ ⟧            // branch if less than
    | ⟦BGE ⟨Label⟩ ⟧            // branch if greater than or equal
    | ⟦BLE ⟨Label⟩ ⟧            // branch if less than or equal
    | ⟦BL ⟨Label⟩ ⟧         // branch and link

    | ⟦LDR ⟨Reg⟩, ⟨Mem⟩ ⟧       // load register from memory
    | ⟦STR ⟨Reg⟩, ⟨Mem⟩ ⟧       // store register to memory

    | ⟦LDMFD ⟨Reg⟩! , {⟨Regs⟩} ⟧    // load multiple fully descending (pop)
    | ⟦STMFD ⟨Reg⟩! , {⟨Regs⟩} ⟧    // store multiple fully descending (push)
    ;

  // Arguments.

  sort Reg  | ⟦R0⟧ | ⟦R1⟧ | ⟦R2⟧ | ⟦R3⟧ | ⟦R4⟧ | ⟦R5⟧ | ⟦R6⟧ | ⟦R7⟧
        | ⟦R8⟧ | ⟦R9⟧ | ⟦R10⟧ | ⟦R11⟧ | ⟦R12⟧ | ⟦SP⟧ | ⟦LR⟧ | ⟦PC⟧ ;

  sort Arg | ⟦⟨Constant⟩⟧ | ⟦⟨Reg⟩⟧ | ⟦⟨Reg⟩, LSL ⟨Constant⟩⟧ | ⟦⟨Reg⟩, LSR ⟨Constant⟩⟧ ;

  sort Mem | ⟦[⟨Reg⟩, ⟨Sign⟩⟨Arg⟩]⟧ ;
  sort Sign | ⟦+⟧ | ⟦-⟧ | ⟦⟧ ;

  sort Regs | ⟦⟨Reg⟩⟧ | ⟦⟨Reg⟩-⟨Reg⟩⟧ | ⟦⟨Reg⟩, ⟨Regs⟩⟧ | ⟦⟨Reg⟩-⟨Reg⟩, ⟨Regs⟩⟧ ;

  sort Constant | ⟦#⟨Integer⟩⟧ | ⟦&⟨Identifier⟩⟧ ; // TODO changed

  // Helper concatenation/flattening of Instructions.
  sort Instructions | scheme ⟦ { ⟨Instructions⟩ } ⟨Instructions⟩ ⟧ ;
  ⟦ {} ⟨Instructions#⟩ ⟧ → # ;
  ⟦ {⟨Instruction#1⟩ ⟨Instructions#2⟩} ⟨Instructions#3⟩ ⟧
    → ⟦ ⟨Instruction#1⟩ {⟨Instructions#2⟩} ⟨Instructions#3⟩ ⟧ ;

  // Helper data structure for list of registers.
  sort Rs | NoRs | MoRs(Reg, Rs) | scheme AppendRs(Rs, Rs) ;
  AppendRs(NoRs, #Rs) → #Rs ;
  AppendRs(MoRs(#Rn, #Rs1), #Rs2) → MoRs(#Rn, AppendRs(#Rs1, #Rs2)) ;

  // Helper conversion from Regs syntax to register list.
  | scheme XRegs(Regs) ;
  XRegs(⟦⟨Reg#r⟩⟧) → MoRs(#r, NoRs) ;
  XRegs(⟦⟨Reg#r1⟩-⟨Reg#r2⟩⟧) → XRegs1(#r1, #r2) ;
  XRegs(⟦⟨Reg#r⟩, ⟨Regs#Rs⟩⟧) → MoRs(#r, XRegs(#Rs)) ;
  XRegs(⟦⟨Reg#r1⟩-⟨Reg#r2⟩, ⟨Regs#Rs⟩⟧) → AppendRs(XRegs1(#r1, #r2), XRegs(#Rs)) ;

  | scheme XRegs1(Reg, Reg) ;
  XRegs1(#r, #r) → MoRs(#r, NoRs) ;
  default XRegs1(#r1, #r2) → XRegs2(#r1, #r2) ;

  | scheme XRegs2(Reg, Reg) ;
  XRegs2(⟦R0⟧, #r2) → MoRs(⟦R0⟧, XRegs1(⟦R1⟧, #r2)) ;
  XRegs2(⟦R1⟧, #r2) → MoRs(⟦R1⟧, XRegs1(⟦R2⟧, #r2)) ;
  XRegs2(⟦R2⟧, #r2) → MoRs(⟦R2⟧, XRegs1(⟦R3⟧, #r2)) ;
  XRegs2(⟦R3⟧, #r2) → MoRs(⟦R3⟧, XRegs1(⟦R4⟧, #r2)) ;
  XRegs2(⟦R4⟧, #r2) → MoRs(⟦R4⟧, XRegs1(⟦R5⟧, #r2)) ;
  XRegs2(⟦R5⟧, #r2) → MoRs(⟦R5⟧, XRegs1(⟦R6⟧, #r2)) ;
  XRegs2(⟦R6⟧, #r2) → MoRs(⟦R6⟧, XRegs1(⟦R7⟧, #r2)) ;
  XRegs2(⟦R7⟧, #r2) → MoRs(⟦R7⟧, XRegs1(⟦R8⟧, #r2)) ;
  XRegs2(⟦R8⟧, #r2) → MoRs(⟦R8⟧, XRegs1(⟦R9⟧, #r2)) ;
  XRegs2(⟦R9⟧, #r2) → MoRs(⟦R9⟧, XRegs1(⟦R10⟧, #r2)) ;
  XRegs2(⟦R10⟧, #r2) → MoRs(⟦R10⟧, XRegs1(⟦R11⟧, #r2)) ;
  XRegs2(⟦R11⟧, #r2) → MoRs(⟦R11⟧, XRegs1(⟦R12⟧, #r2)) ;
  XRegs2(⟦R12⟧, #r2) → MoRs(⟦R12⟧, NoRs) ;
  XRegs1(⟦SP⟧, #r2) → error⟦MinARM32 error: Cannot use SP in Regs range.⟧ ;
  XRegs1(⟦LR⟧, #r2) → error⟦MinARM32 error: Cannot use LR in Regs range.⟧ ;
  XRegs1(⟦PC⟧, #r2) → error⟦MinARM32 error: Cannot use PC in Regs range.⟧ ;

  // Helpers to insert computed assembly constants.
  sort Constant | scheme Immediate(Computed) | scheme Reference(Computed) ;
  Immediate(#x) → ⟦#⟨INT#x⟩⟧ ;
  Reference(#id) → ⟦&⟨Identifier#id⟩⟧ ; // TODO look at

  sort Mem | scheme FrameAccess(Computed) ;
  FrameAccess(#x)
    → FrameAccess1(#x, ⟦ [R12, ⟨Constant Immediate(#x)⟩] ⟧, ⟦ [R12, -⟨Constant Immediate(⟦0-#x⟧)⟩] ⟧) ;
  | scheme FrameAccess1(Computed, Mem, Mem) ;
  FrameAccess1(#x, #pos, #neg) → FrameAccess2(⟦ #x ≥ 0 ? #pos : #neg ⟧) ;
  | scheme FrameAccess2(Computed) ;
  FrameAccess2(#mem) → #mem ;

  sort Instruction | scheme AddConstant(Reg, Reg, Computed) ;
  AddConstant(#Rd, #Rn, #x)
    → AddConstant1(#x,
           ⟦ ADD ⟨Reg#Rd⟩, ⟨Reg#Rn⟩, ⟨Constant Immediate(#x)⟩ ⟧,
           ⟦ SUB ⟨Reg#Rd⟩, ⟨Reg#Rn⟩, ⟨Constant Immediate(⟦0-#x⟧)⟩ ⟧) ;
  | scheme AddConstant1(Computed, Instruction, Instruction) ;
  AddConstant1(#x, #pos, #neg) → AddConstant2(⟦ #x ≥ 0 ? #pos : #neg ⟧) ;
  | scheme AddConstant2(Computed) ;
  AddConstant2(#add) → #add ;


  ////////////////////////////////////////////////////////////////////////
  // 3. COMPILER FROM MiniC TO MinARM32
  ////////////////////////////////////////////////////////////////////////

  // HACS doesn't like to compile with Computed sort
  // unless there exists a scheme that can generate Computed
  sort Computed | scheme Dummy ;
  Dummy → ⟦ 0 ⟧;

  // MAIN SCHEME

  sort Instructions  |  scheme Compile(Program) ;
  Compile(#1) → P2(P1(#1), #1) ;

  // PASS 1

  // Result sort for first pass, with join operation.
  sort After1 | Data1(Instructions, FT) | scheme Join1(After1, After1) ;
  Join1(Data1(#1, #ft1), Data1(#2, #ft2))
    → Data1(⟦ { ⟨Instructions#1⟩ } ⟨Instructions#2⟩ ⟧, AppendFT(#ft1, #ft2)) ;

  // Function to return type environment (list of pairs with append).
  sort FT | NoFT | MoFT(Identifier, Type, FT) | scheme AppendFT(FT, FT) ;
  AppendFT(NoFT, #ft2) → #ft2 ;
  AppendFT(MoFT(#id1, #T1, #ft1), #ft2) → MoFT(#id1, #T1, AppendFT(#ft1, #ft2)) ;

  // Pass 1 recursion.
  sort After1 | scheme P1(Program) ;
  P1(⟦⟨Declarations#Ds⟩⟧) → P1Ds(#Ds) ;

  sort After1 | scheme P1Ds(Declarations);  // Def. \ref{def:P}.
  P1Ds(⟦⟨Declaration#D⟩ ⟨Declarations#Ds⟩⟧) → Join1(D(#D), P1Ds(#Ds)) ;
  P1Ds(⟦⟧) → Data1(⟦⟧, NoFT) ;

  // \sem{D} scheme (Def. \ref{def:D}).

  sort After1 | scheme D(Declaration) ;
  D(⟦ function ⟨Type#T⟩ f ⟨ArgumentSignature#As⟩ { ⟨Statements#S⟩ } ⟧)
    → Data1(⟦⟧, MoFT(⟦f⟧, #T, NoFT)) ;

  // PASS 2

  // Pass 2 strategy: first load type environment $ρ$ then tail-call recursion.
  sort Instructions | scheme P2(After1, Program) ;
  P2(Data1(#1, #ft1), #P) → P2Load(#1, #ft1, #P) ;

  // Type environment ($ρ$) is split in two components (by used sorts).
  attribute ↓ft{Identifier : Type} ;    // map from function name to return type
  attribute ↓vt{Identifier : Local} ;   // map from local variable name to type\&location
  sort Local | RegLocal(Type, Reg) | FrameLocal(Type, Computed) ;  // type\&location

  // Other inherited attributes.
  attribute ↓return(Label) ;        // label of return code
  attribute ↓true(Label) ;      // label to jump for true result
  attribute ↓false(Label) ;     // label to jump for false result
  attribute ↓value(Reg) ;       // register for expression result
  attribute ↓offset(Computed) ;     // frame offset for first unused local
  attribute ↓unused(Rs) ;       // list of unused registers
  // Added attributes
  attribute ↑used(Reg) ; // holds the register the expression actually used
  attribute ↓memlabel(Label) ;
  attribute ↓argn(Rs) ;   // register list for arguments for pushing arguments for function call

  // Pass 2 Loader: extract type environment $ρ$ and emit pass 1 directives.
  sort Instructions | scheme P2Load(Instructions, FT, Program) ↓ft ↓vt ;
  P2Load(#is, MoFT(⟦f⟧, #T, #ft), #P) → P2Load(#is, #ft, #P) ↓ft{⟦f⟧ : #T} ;
  P2Load(#is, NoFT, #P) → ⟦ { ⟨Instructions#is⟩ } ⟨Instructions P(#P)⟩ ⟧ ;

  // Pass 2 recursion.
  sort Instructions | scheme P(Program) ↓ft ↓vt ;
  P(⟦ ⟨Declarations#Ds⟩ ⟧) → Ds(#Ds) ;

  sort Instructions | scheme Ds(Declarations) ↓ft ↓vt ;
  Ds(⟦ ⟨Declaration#D⟩ ⟨Declarations#Ds⟩ ⟧) → ⟦ { ⟨Instructions F(#D)⟩ } ⟨Instructions Ds(#Ds)⟩ ⟧ ;
  Ds(⟦⟧) → ⟦⟧ ;

  // \sem{F} scheme (Def. \ref{def:F}), with argument signature iteration helpers.

  sort Instructions | scheme F(Declaration) ↓ft ↓vt ;
  F(⟦ function ⟨Type#T⟩ f ⟨ArgumentSignature#AS⟩ { ⟨Statements#S⟩ } ⟧) → ⟦
    f   STMFD SP!, {R4-R11,LR}
        MOV R12, SP
        { ⟨Instructions AS(#AS, XRegs(⟦R0-R3⟧), #S) ↓return(⟦L⟧)⟩ }
    L   MOV SP, R12
        LDMFD SP!, {R4-R11,PC}
  ⟧ ;

  sort Instructions | scheme AS(ArgumentSignature, Rs, Statements) ↓ft ↓vt ↓return ;
  AS(⟦ () ⟧, #Rs, #S) → S(#S) ↓offset(⟦0-4⟧) ↓unused(XRegs(⟦R4-R11⟧)) ;
  AS(⟦ ( ⟨Type#T⟩ a ⟨TypeIdentifierTail#TIT⟩ ) ⟧, MoRs(#r, #Rs), #S)
    → AS2(#TIT, #Rs, #S) ↓vt{⟦a⟧ : RegLocal(#T, #r)} ;

  sort Instructions | scheme AS2(TypeIdentifierTail, Rs, Statements) ↓ft ↓vt ↓return ;
  AS2(⟦ ⟧, #Rs, #S) → S(#S) ↓offset(⟦0-4⟧) ↓unused(XRegs(⟦R4-R11⟧)) ;
  AS2(⟦ , ⟨Type#T⟩ a ⟨TypeIdentifierTail#TIT⟩ ⟧, MoRs(#r, #Rs), #S)
    → AS2(#TIT, #Rs, #S) ↓vt{⟦a⟧ : RegLocal(#T, #r)} ;
  AS2(⟦ , ⟨Type#T⟩ a ⟨TypeIdentifierTail#TIT⟩ ⟧, NoRs, #S)
    → error⟦More than four arguments to function not allowed.⟧ ;


  ///// HELPER SCHEMES /////
  sort Arg | scheme IdLocation(Identifier)↓vt;
  IdLocation(#I1)↓vt{#I1: RegLocal(#T1, #R1)} → ⟦⟨Reg #R1⟩⟧; // In a Register

  sort Reg | scheme IdReg(Identifier)↓vt;
  IdReg(#I1)↓vt{#I1: RegLocal(#T1, #R1)} → #R1; // In a Registeri

  sort Computed | scheme DetermineOffset(Type, Computed);
  DetermineOffset(⟦*⟨Type#⟩⟧, #n) → ⟦4 + #n⟧;
  DetermineOffset(⟦int⟧, #n) → ⟦#n -4⟧;
  DetermineOffset(⟦char⟧, #n) → ⟦#n - 4⟧;
  DetermineOffset(#, #n) → #n;

  sort Instructions | scheme PointerExpr(Expression);
  PointerExpr(⟦⟨Identifier#I1⟩⟧)↓vt{#I1 : FrameLocal(#T1, #r)}↓value(#RR) → ⟦
   LDR ⟨Reg#RR⟩, [R12, ⟨Constant Reference(#I1)⟩]
   LDR ⟨Reg#RR⟩, [⟨Reg#RR⟩, #0]
  ⟧↑used(#RR) ;
  PointerExpr(⟦⟨Identifier#I1⟩ + ⟨INT#n⟩⟧)↓vt{#I1 : FrameLocal(#T1, #r)}↓value(#RR) → ⟦
    LDR ⟨Reg#RR⟩, [R12, ⟨Constant Reference(#I1)⟩]
    LDR ⟨Reg#RR⟩, [⟨Reg#RR⟩, ⟨Constant GetMultiple(#T1, #n)⟩]
  ⟧↑used(#RR) ;
  PointerExpr(⟦⟨Identifier#I1⟩⟧)↓vt{#I1 : RegLocal(#T1, #r)}↓value(#RR) → ⟦
   LDR ⟨Reg#RR⟩, [⟨Reg#r⟩, #0]
  ⟧↑used(#RR) ;

  sort Constant | GetMultiple(Type, INT);
  GetMultiple(#t, #n) → ⟦⟨Constant Immediate(MultiplyComputed(#n, GetTypeSize(#t)))⟩⟧;

  sort Computed | MultiplyComputed(Computed, Computed) | AddComputed(Computed, Computed);
  MultiplyComputed(#n1, #n2) → ⟦$#n1 * $#n2⟧;
  AddComputed(#n1, #n2) → ⟦$#n1 + $#n2⟧;

  // Size of types
   sort Computed | GetSizeOf(Type);
  GetSizeOf(⟦char⟧) → ⟦0x1⟧;
  GetSizeOf(⟦int⟧) → ⟦0x4⟧;
  GetSizeOf(⟦*⟨Type#⟩⟧) → ⟦0x4⟧;
  GetSizeOf(⟦⟨Type#T⟩ ( ⟨TypeList#L⟩ )⟧) → AddComputed(GetTypeSize(#T), GetTypeListSize(#L));

  sort Computed | GetTypeSize(Type);
  GetTypeSize(⟦char⟧) → ⟦0x1⟧;
  GetTypeSize(⟦int⟧) → ⟦0x4⟧;
  GetTypeSize(⟦*char⟧) → ⟦0x1⟧;
  GetTypeSize(⟦*int⟧) → ⟦0x4⟧;
  GetTypeSize(⟦**⟨Type#⟩⟧) → ⟦0x4⟧;
  GetTypeSize(⟦⟨Type#T⟩ ( ⟨TypeList#L⟩ )⟧) → AddComputed(GetTypeSize(#T), GetTypeListSize(#L));
  sort Computed | GetTypeListSize(TypeList);
  GetTypeListSize(⟦ ⟨Type#T⟩ ⟨TypeListTail#Lt⟩ ⟧)→ AddComputed(GetTypeSize(#T), GetTypeListTailSize(#Lt));
  GetTypeListSize(⟦⟧)→ ⟦0⟧;
  sort Computed | GetTypeListTailSize(TypeListTail);
  GetTypeListTailSize(⟦ , ⟨Type#T⟩ ⟨TypeListTail#Lt⟩ ⟧) → AddComputed(GetTypeSize(#T), GetTypeListTailSize(#Lt));
  GetTypeListTailSize(⟦⟧) → ⟦0⟧;

  // Null value of types
  sort Computed | GetNull(Type);
  GetNull(⟦char⟧) → ⟦0⟧;
  GetNull(⟦int⟧) → ⟦0⟧;
  GetNull(⟦*⟨Type#⟩⟧) → ⟦0⟧;
  GetNull(⟦ ( ⟨Type#⟩ )⟧) → ⟦0⟧;
  GetNull(⟦ ⟨Type#t⟩ ( ⟨TypeList#tl⟩ )⟧) → ⟦0⟧;

  sort Instructions | LoadPointerToReg(Expression);
  LoadPointerToReg(⟦⟨Identifier#I1⟩⟧)↓vt{:#vCopy}↓vt{#I1 : FrameLocal(#T1, #r)}
  ↓value(#UR1)↓unused(MoRs(#R1, #Rs)) → ⟦
    MOV ⟨Reg #R1⟩, ⟨Constant Reference(#I1)⟩
    STR ⟨Reg #UR1⟩, [⟨Reg #R1⟩, #0]
  ⟧↑used(#R1);
  LoadPointerToReg(⟦⟨Identifier#I1⟩ + ⟨INT#n⟩⟧)↓vt{:#vCopy}↓vt{#I1 : FrameLocal(#T1, #r)}
  ↓value(#UR1)↓unused(MoRs(#R1, #Rs)) → ⟦
    MOV ⟨Reg #R1⟩,  ⟨Constant Reference(#I1)⟩
    STR ⟨Reg #UR1⟩, [⟨Reg #R1⟩, ⟨Constant GetMultiple(#T1, #n)⟩]
  ⟧↑used(#R1);

  sort Computed | GetEListSize(ExpressionList);
  GetEListSize(⟦⟧) → ⟦0⟧;
  GetEListSize(⟦⟨Expression#E1⟩ ⟨ExpressionListTail#Et⟩⟧) → AddComputed(⟦1⟧, GetEListSize(#Et));

  sort Reg | NumToReg(Computed);
  NumToReg(#n) → ⟦⟨Reg#n⟩⟧;

  sort Instructions | ↑used ;
  sort Instructions | scheme S(Statements) ↓ft ↓vt ↓return ↓unused ↓offset ↓true ↓false ↓value ;
  sort Instructions | scheme E(Expression) ↓ft ↓vt ↓return ↓unused ↓offset ↓true ↓false ↓value ;

  // Empty case
  S(⟦⟧) → ⟦⟧;

  // Test function args case

  // TODO: spill something!
  S(#)↓unused(NoRs) ↓vt{:#vCopy} → ⟦
    B Heyheyhey_spill_something
  ⟧ ;
 
  // Nesting, pass a copy of ft and vt to nested block
  // any variable/functions defined inside dont change the outer ft or vt
  S(⟦ { ⟨Statements#S1⟩ } ⟨Statements#S2⟩ ⟧)↓vt{:#vCopy} ↓ft{:#fCopy} ↓unused(#Rs) → ⟦
    { ⟨Instructions S(#S1)↓vt{:#vCopy} ↓unused(#Rs) ↓ft{:#fCopy}⟩ }
    { ⟨Instructions S(#S2)↓vt{:#vCopy} ↓unused(#Rs) ↓ft{:#fCopy}⟩ }
  ⟧ ;

  // Variable declaration

  // Pointer assignment
  /*S(⟦ var *⟨Type#T1⟩ ⟨Identifier#I1⟩ ; ⟨Statements#S2⟩ ⟧)↓vt{:#vCopy}↓ft{:#fCopy}
  ↓unused(#Rs)↓offset(#o) → ⟦
    DEF ⟨Identifier #I1⟩ = ⟨INT #o⟩
    { ⟨Instructions S(#S2)↓ft{:#fCopy}↓unused(#Rs)↓vt{:#vCopy}↓vt{#I1: FrameLocal(#T1, #o)}↓offset(DetermineOffset(#T1, #o))⟩ }
  ⟧ ;*/


  // add to vt and initialize it according to type
  S(⟦ var ⟨Type#T1⟩ ⟨Identifier#I1⟩ ; ⟨Statements#S2⟩ ⟧)↓vt{:#vCopy}↓unused(#Rs)
  ↓ft{:#fCopy}
  ↓offset(#o) → ⟦
    SUB SP, SP, #4
    DEF ⟨Identifier #I1⟩ = ⟨INT #o⟩
    { ⟨Instructions S(#S2)↓ft{:#fCopy}↓unused(#Rs)↓vt{:#vCopy}↓vt{#I1: FrameLocal(#T1, #o)}↓offset(DetermineOffset(#T1, #o))⟩ }
  ⟧ ;

  // Frame local assignment of a variable. This case we are out of registers
 /* S(⟦ var ⟨Type#T1⟩ ⟨Identifier#I1⟩ ; ⟨Statements#S2⟩ ⟧)↓ft{:#fCopy}↓vt{:#vCopy}
  ↓unused(NoRs)↓offset(#o) → ⟦
    DEF ⟨Identifier #I1⟩ = ⟨INT #o⟩
    { ⟨Instructions S(#S2)↓ft{:#fCopy}↓unused(NoRs)↓vt{:#vCopy}↓vt{#I1: FrameLocal(#T1, #o)}↓offset(DetermineOffset(#T1, #o))⟩ }
  ⟧ ;*/


  // Assignment

  // Pointer dereference assignment
  // Pointer in register

  // Helper to put expression in register we plan to store at memory
  sort Instructions | scheme AssignmentH(Expression, Reg)↓ft ↓vt ↓return ↓unused ↓offset ↓true ↓false ↓value ;

  S(⟦ *⟨Expression#E1⟩ = ⟨Expression#E2⟩ ; ⟨Statements#S2⟩ ⟧)
  ↓unused(MoRs(#R1, #Rs))
  ↓vt{:#vCopy}↓ft{:#fCopy} → ⟦
    { ⟨Instructions AssignmentH(#E2, #R1)↓ft{:#fCopy}↓vt{:#vCopy}↓unused(#Rs)⟩ }
    { ⟨Instructions LoadPointerToReg(#E1)↓ft{:#fCopy}↓vt{:#vCopy}↓unused(#Rs)↓value(#R1)⟩ }
    { ⟨Instructions S(#S2)↓unused(MoRs(#R1, #Rs))↓ft{:#fCopy}↓vt{:#vCopy}⟩ }
  ⟧;

  // Literal into register to prevent a synthesized
  AssignmentH(⟦ ⟨Identifier#I⟩ ⟧, #reg)↓vt{#I: RegLocal(#T, #R1)} → ⟦
    MOV ⟨Reg #reg⟩, ⟨Reg #R1⟩
  ⟧↑used(#reg);

  AssignmentH(#E1, #reg)↓vt{:#vCopy}↓unused(#Rs)↓ft{:#fCopy} → ⟦
    { ⟨Instructions E(#E1)↓ft{:#fCopy}↓unused(#Rs)↓value(#reg)↓vt{:#vCopy}⟩ }
  ⟧;

  S(⟦ ⟨Identifier#I1⟩ = ⟨Expression#E2⟩ ; ⟨Statements#S2⟩ ⟧)
  ↓unused(#Rs)
  ↓ft{:#fCopy}
  ↓vt{:#vCopy}↓vt{#I1 : RegLocal(#T1, #r)} → ⟦
    { ⟨Instructions E(#E2)↓ft{:#fCopy}↓unused(#Rs)↓value(#r)↓vt{:#vCopy}⟩ }
    { ⟨Instructions StoreIfNotArg(#r, #I1)⟩ }
    { ⟨Instructions S(#S2)↓ft{:#fCopy}↓unused(#Rs)↓vt{:#vCopy}⟩ }
  ⟧;

  sort Instructions | scheme StoreIfNotArg(Reg, Identifier);
  StoreIfNotArg(⟦R0⟧, #I) → ⟦⟧;
  StoreIfNotArg(⟦R1⟧, #I) → ⟦⟧;
  StoreIfNotArg(⟦R2⟧, #I) → ⟦⟧;
  StoreIfNotArg(⟦R3⟧, #I) → ⟦⟧;
  StoreIfNotArg(#r, #I) → ⟦
    STR ⟨Reg#r⟩, [R12, ⟨Constant Reference(#I)⟩]
  ⟧;

  // Frame local case
  S(⟦ ⟨Identifier#I1⟩ = ⟨Expression#E2⟩ ; ⟨Statements#S2⟩ ⟧)
  ↓unused(MoRs(#R2,#Rs))
  ↓ft{:#fCopy}
  ↓vt{:#vCopy}↓vt{#I1 : FrameLocal(#T1, #f)} → ⟦
    { ⟨Instructions E(#E2)↓ft{:#fCopy}↓unused(#Rs)↓value(#R2)↓vt{:#vCopy}⟩ }

    STR ⟨Reg #R2⟩, [R12, ⟨Constant Reference(#I1)⟩]
    
    { ⟨Instructions S(#S2)↓ft{:#fCopy}↓unused(MoRs(#R2,#Rs))↓vt{:#vCopy}⟩ }
  ⟧;

  sort Instructions | scheme AddH(Instructions, Instructions, Reg) ;

  E(⟦ ⟨Expression#E1⟩ + ⟨Expression#E2⟩ ⟧)↓unused(MoRs(#R1, MoRs(#R2, #Rs)))
  ↓ft{:#fCopy}
  ↓value(#Rv) ↓vt{:#v} → AddH(
    ⟦⟨Instructions E(#E1)↓ft{:#fCopy}↓value(#R1) ↓unused(#Rs) ↓vt{:#v}⟩⟧,
    ⟦⟨Instructions E(#E2)↓ft{:#fCopy}↓value(#R2) ↓unused(#Rs) ↓vt{:#v}⟩⟧,
    #Rv
  )↑used(#Rv) ;

  AddH(⟦ ⟨Instructions#I1⟩ ⟧↑used(#R1), ⟦ ⟨Instructions#I2⟩ ⟧↑used(#R2), #Rv) → ⟦
    { ⟨Instructions#I1⟩ }
    { ⟨Instructions#I2⟩ }
    ADD ⟨Reg#Rv⟩, ⟨Reg#R1⟩, ⟨Reg#R2⟩
  ⟧↑used(#Rv) ;

  // if statement
  S(⟦ if ( ⟨Expression#E1⟩ ) {⟨Statements#S1⟩} else {⟨Statements#S2⟩} ⟨Statements#S3⟩ ⟧)
  ↓ft{:#fCopy}
  ↓unused(MoRs(#R1, #Rs)) → ⟦
    ifcond    { ⟨Instructions GenComp(#E1)↓ft{:#fCopy}↓unused(#Rs)↓true(⟦iftrue⟧) ↓false(⟦iffalse⟧) ↓value(#R1)⟩ }
    iftrue    { ⟨Instructions S(#S1)↓ft{:#fCopy}↓unused(#Rs)⟩ }
    B ifend
    iffalse   { ⟨Instructions S(#S2)↓ft{:#fCopy}↓unused(#Rs)⟩ }
    ifend     { ⟨Instructions S(#S3)↓ft{:#fCopy}↓unused(#Rs)⟩ }
  ⟧;

  S(⟦ if ( ⟨Expression#E1⟩ ) {⟨Statements#S1⟩} ⟨Statements#S2⟩ ⟧)
  ↓ft{:#fCopy}
  ↓unused(MoRs(#R1, #Rs)) → ⟦
    ifcond    { ⟨Instructions GenComp(#E1)↓ft{:#fCopy}↓unused(#Rs)↓true(⟦iftrue⟧) ↓false(⟦ifend⟧) ↓value(#R1)⟩ }
    iftrue    { ⟨Instructions S(#S1)↓ft{:#fCopy}↓unused(#Rs)⟩ }
    ifend     { ⟨Instructions S(#S2)↓ft{:#fCopy}↓unused(#Rs)⟩ }
  ⟧;

  // while loop
  S(⟦ while ( ⟨Expression#E1⟩ ) {⟨Statements#S1⟩} ⟨Statements#S2⟩ ⟧)
  ↓ft{:#fCopy}
  ↓unused(MoRs(#R1, #Rs)) → ⟦
    B whilecond
    whiletrue
    { ⟨Instructions S(#S1)↓ft{:#fCopy}↓unused(#Rs)⟩ }
    whilecond  
    { ⟨Instructions GenComp(#E1)↓ft{:#fCopy}↓unused(#Rs) ↓true(⟦whiletrue⟧) ↓false(⟦whileend⟧) ↓value(#R1)⟩ }
    whileend
    { ⟨Instructions S(#S2)↓ft{:#fCopy}↓unused(#Rs)⟩ }
  ⟧;

  // return Statements
  S(⟦ return ⟨Identifier#I1⟩ ; ⟨Statements#S2⟩ ⟧)
  ↓ft{:#fCopy}
  ↓return(#L)
  ↓vt{#I1: RegLocal(#T, #R)} → ⟦
    MOV R0, ⟨Reg#R⟩
    B ⟨Label #L⟩
  ⟧ ;

  S(⟦ return ⟨Expression#E1⟩ ; ⟨Statements#S2⟩ ⟧)
  ↓ft{:#fCopy} ↓vt{:#vCopy}
  ↓return(#L) ↓unused(MoRs(#R1, #Rs)) → ⟦
    { ⟨Instructions E(#E1)↓vt{:#vCopy}↓ft{:#fCopy}↓value(#R1) ↓unused(#Rs)⟩ }
    MOV R0, ⟨Reg#R1⟩
    B ⟨Label #L⟩
  ⟧ ;



  // Pointer return
  S(⟦ return ⟨Expression#E1⟩ ; ⟨Statements#S2⟩ ⟧)
  ↓ft{:#fCopy}
  ↓return(#L) ↓unused(MoRs(#R1, #Rs)) → ⟦
    { ⟨Instructions E(#E1)↓ft{:#fCopy}↓value(#R1) ↓unused(#Rs)⟩ }
    MOV R0, ⟨Reg#R1⟩
    B ⟨Label #L⟩
  ⟧ ;

  // Literals and Identifiers
  E(⟦ ⟨Integer#I⟩ ⟧)↓value(#R1) → ⟦
    MOV ⟨Reg#R1⟩, #⟨Integer#I⟩
  ⟧↑used(Reg#R1) ;

  E(⟦ ⟨String#S⟩ ⟧)↓value(#R1) → ⟦
    string DCS ⟨String#S⟩
    MOV ⟨Reg#R1⟩, &string
  ⟧↑used(#R1) ;

  E(⟦ ⟨Identifier#I⟩ ⟧)↓vt{#I: RegLocal(#T, #reg)} 
  ↓value(#R1) → ⟦⟧↑used(#reg) ;

  E(⟦ ⟨Identifier#I⟩ ⟧)↓vt{#I: FrameLocal(#T, #off)} ↓value(#R1) → ⟦
    LDR ⟨Reg#R1⟩, [R12, ⟨Constant Reference(#I)⟩]
  ⟧↑used(#R1) ;

  E(⟦ ⟨Identifier#I⟩ ⟧)↓value(#R1)  → ⟦⟧↑used(#R1);

  // ========================================================================
  // function calls
  // ========================================================================

  sort Instructions | BringNRegBack(Computed);
  BringNRegBack(⟦0⟧) → ⟦⟧;
  BringNRegBack(#n) → ⟦
    {⟨Instructions BringNRegBack(⟦#n-1⟧)⟩}
    {⟨Instructions ReloadRegAfterCall(NumToReg(#n))⟩}

  ⟧;


  E(⟦ f ( ⟨ExpressionList#EL1⟩ ) ⟧)↓ft{:#fCopy}↓unused(#Rs) ↓value(#R)↓vt{:#v} → ⟦

    { ⟨Instructions EList(#EL1)↓ft{:#fCopy}↓vt{:#v}↓unused(#Rs)↓argn(XRegs(⟦R0-R3⟧))⟩ }
    BL f
    MOV ⟨Reg#R⟩, R0

    { ⟨Instructions RevertRegs(#EL1)↓vt{:#v}↓argn(XRegs(⟦R0-R3⟧))⟩ }

  ⟧↑used(#R) ;
  
  sort Instructions | scheme EList(ExpressionList)↓ft ↓vt ↓return ↓unused ↓offset ↓true ↓false ↓value ↓argn; 
  EList(⟦ ⟧) → ⟦ ⟧ ;
  // Optimization, don't MOV unless we have to i.e we're using R0 and was the callees R0
  /*EList(⟦ ⟨Identifier#I1⟩ ⟨ExpressionListTail#Et⟩ ⟧)↓ft{:#fCopy}↓unused(MoRs(#R2, #Rs)) 
  ↓argn(MoRs(#R, #Rn))↓vt{:#v}↓vt{#I1:RegLocal(#T,#R)} → ⟦
    {⟨Instructions EListT(#Et)↓vt{:#v}↓unused(MoRs(#R2, #Rs))↓ft{:#fCopy}↓argn(#Rn) ⟩}
  ⟧;*/
  EList(⟦ ⟨Identifier#I1⟩ ⟨ExpressionListTail#Et⟩ ⟧)↓ft{:#fCopy}↓unused(MoRs(#R2, #Rs)) 
  ↓argn(MoRs(#R, #Rn))↓vt{:#v}↓vt{#I1:RegLocal(#T,#reg)} → ⟦
    {⟨Instructions SaveRegBeforeCall(#R)⟩}
    MOV ⟨Reg#R⟩, ⟨Reg#reg⟩
    {⟨Instructions EListT(#Et)↓vt{:#v}↓unused(MoRs(#R2, #Rs))↓ft{:#fCopy}↓argn(#Rn) ⟩}
  ⟧;
  EList(⟦ ⟨Expression#E1⟩ ⟨ExpressionListTail#Et⟩ ⟧)↓ft{:#fCopy}↓unused(MoRs(#R2, #Rs)) 
  ↓argn(MoRs(#R, #Rn))↓vt{:#v} → ⟦
    {⟨Instructions E(#E1)↓vt{:#v}↓unused(#Rs)↓ft{:#fCopy}↓value(#R2)⟩}
    {⟨Instructions SaveRegBeforeCall(#R)⟩}
    MOV ⟨Reg#R⟩, ⟨Reg#R2⟩
    {⟨Instructions EListT(#Et)↓vt{:#v}↓unused(MoRs(#R2, #Rs))↓ft{:#fCopy}↓argn(#Rn) ⟩}
  ⟧↑used(#R);

  sort Instructions | scheme EListT(ExpressionListTail) ↓ft ↓vt ↓return ↓unused ↓offset ↓true ↓false ↓value ↓argn ;
  EListT(⟦ ⟧) → ⟦ ⟧ ;
  /*EListT(⟦ , ⟨Identifier#I1⟩ ⟨ExpressionListTail#Et⟩ ⟧)↓ft{:#fCopy}↓unused(MoRs(#R2, #Rs)) 
  ↓argn(MoRs(#R, #Rn))↓vt{:#v}↓vt{#I1:RegLocal(#T,#R)} → ⟦
    {⟨Instructions EListT(#Et)↓vt{:#v}↓unused(MoRs(#R2, #Rs))↓ft{:#fCopy}↓argn(#Rn) ⟩}
  ⟧;*/
  EListT(⟦ , ⟨Identifier#I1⟩ ⟨ExpressionListTail#Et⟩ ⟧)↓ft{:#fCopy}↓unused(MoRs(#R2, #Rs)) 
  ↓argn(MoRs(#R, #Rn))↓vt{:#v}↓vt{#I1:RegLocal(#T,#reg)} → ⟦
    {⟨Instructions SaveRegBeforeCall(#R)⟩}
    MOV ⟨Reg#R⟩, ⟨Reg#reg⟩
    {⟨Instructions EListT(#Et)↓vt{:#v}↓unused(MoRs(#R2, #Rs))↓ft{:#fCopy}↓argn(#Rn) ⟩}
  ⟧;
  EListT(⟦ , ⟨Expression#E1⟩ ⟨ExpressionListTail#Et⟩ ⟧)↓ft{:#fCopy}↓unused(MoRs(#R2, #Rs)) 
   ↓argn(MoRs(#R, #Rn))↓vt{:#v} → ⟦
    {⟨Instructions E(#E1)↓vt{:#v}↓unused(#Rs)↓ft{:#fCopy}↓value(#R2)⟩}
    {⟨Instructions SaveRegBeforeCall(#R)⟩}
    MOV ⟨Reg#R⟩, ⟨Reg#R2⟩
    {⟨Instructions EListT(#Et)↓vt{:#v}↓unused(MoRs(#R2, #Rs))↓ft{:#fCopy}↓argn(#Rn) ⟩}
  ⟧↑used(#R);

  sort Instructions | scheme RevertRegs(ExpressionList) ↓argn ↓vt;
  RevertRegs(⟦ ⟧) → ⟦ ⟧ ;
  /*RevertRegs(⟦ ⟨Identifier#I1⟩ ⟨ExpressionListTail#Et⟩ ⟧)
  ↓argn(MoRs(#R, #Rn))↓vt{:#v}↓vt{#I1:RegLocal(#T,#R)} → ⟦
    {⟨Instructions RevertRegsTail(#Et)↓argn(#Rn)↓vt{:#v}⟩ }
  ⟧;*/
  RevertRegs(⟦ ⟨Expression#E1⟩ ⟨ExpressionListTail#Et⟩ ⟧)
  ↓argn(MoRs(#R, #Rn))↓vt{:#v} → ⟦
    {⟨Instructions RevertRegsTail(#Et)↓argn(#Rn)↓vt{:#v}⟩ }
    {⟨Instructions ReloadRegAfterCall(#R)⟩}
  ⟧;

  sort Instructions | scheme RevertRegsTail(ExpressionListTail) ↓argn ↓vt;
  RevertRegsTail(⟦ ⟧) → ⟦ ⟧ ;
  /*RevertRegsTail(⟦ , ⟨Identifier#I1⟩ ⟨ExpressionListTail#Et⟩ ⟧)
  ↓argn(MoRs(#R, #Rn))↓vt{:#v}↓vt{#I1:RegLocal(#T,#R)} → ⟦
     {⟨Instructions RevertRegsTail(#Et)↓argn(#Rn)↓vt{:#v}⟩ }
  ⟧;*/
  RevertRegsTail(⟦ , ⟨Expression#E1⟩ ⟨ExpressionListTail#Et⟩ ⟧)
  ↓argn(MoRs(#R, #Rn))↓vt{:#v} → ⟦
    {⟨Instructions RevertRegsTail(#Et)↓argn(#Rn)↓vt{:#v}⟩ }
    {⟨Instructions ReloadRegAfterCall(#R)⟩}
  ⟧;



  // Helper to determine if we need to store an argument into a function
  sort Instructions | scheme SaveRegBeforeCall(Reg) | ReloadRegAfterCall(Reg);
  SaveRegBeforeCall(⟦⟨Reg #r⟩⟧) → ⟦
    SUB SP, SP, #4
    STR ⟨Reg #r⟩, [SP, #0]
  ⟧;

  ReloadRegAfterCall(⟦⟨Reg #r⟩⟧) → ⟦
    LDR ⟨Reg #r⟩, [SP, #0]
    ADD SP, SP, #4
  ⟧;

  // ========================================================================
  // null and sizeof
  // #RR is the return register
  // ========================================================================
  E(⟦ null ( ⟨Type#T1⟩ ) ⟧)↓value(#RR) → ⟦
    MOV ⟨Reg#RR⟩, ⟨Constant Immediate(GetNull(#T1))⟩
  ⟧↑used(#RR) ;

  E(⟦ sizeof ( ⟨Type#T1⟩ )⟧)↓value(#RR) → ⟦
    MOV ⟨Reg#RR⟩, ⟨Constant Immediate(GetSizeOf(#T1))⟩
  ⟧↑used(#RR) ;

  // Unary exclamation point
  // value 0 if its operand is true (nonzero), the value 1 if its operand is false (0).
  E(⟦ ! ⟨Expression#E1⟩ ⟧)↓unused(#Rs) ↓value(#R1) → ⟦
    { ⟨Instructions E(#E1)↓unused(#Rs) ↓value(#R1)⟩ }
    CMP ⟨Reg#R1⟩, ⟨Constant Immediate(⟦0⟧)⟩
    MOV ⟨Reg#R1⟩, ⟨Constant Immediate(⟦1⟧)⟩
    BGT notzero
    MOV ⟨Reg#R1⟩, ⟨Constant Immediate(⟦0⟧)⟩
    notzero
  ⟧ ;

  // Unary minus - changes sign of operand (subtract from 0)
  E(⟦ - ⟨Identifier#I⟩ ⟧)↓unused(#Rs) ↓value(#R1) ↓vt{#I: RegLocal(#T, #R)} → ⟦
    RSB ⟨Reg#R1⟩, ⟨Reg#R⟩, #0
  ⟧↑used(#R1) ;

  E(⟦ - ⟨Expression#E1⟩ ⟧)↓unused(MoRs(#R, #Rs)) ↓value(#R1) ↓vt{:#v} ↓ft{:#f} → ⟦
    { ⟨Instructions E(#E1)↓value(#R) ↓unused(#Rs) ↓vt{:#v} ↓ft{:#f}⟩ }
    RSB ⟨Reg#R1⟩, ⟨Reg#R⟩, #0
  ⟧↑used(#R1) ;
  
  // Unary plus does nothing
  // see: http://download.mikroe.com/documents/compilers/mikroc/arm/help/unary_operators.htm
  E(⟦ + ⟨Identifier#I⟩ ⟧)↓unused(#Rs) ↓value(#R1) ↓vt{#I: RegLocal(#T, #R)} → ⟦
    MOV ⟨Reg#R1⟩, ⟨Reg#R⟩
  ⟧↑used(#R1) ;
  
  E(⟦ + ⟨Expression#E1⟩ ⟧)↓unused(MoRs(#R, #Rs)) ↓value(#R1) ↓vt{:#v} → ⟦
    { ⟨Instructions E(#E1)↓value(#R) ↓unused(#Rs) ↓vt{:#v}⟩ }
    MOV ⟨Reg#R1⟩, ⟨Reg#R⟩
  ⟧↑used(#R1) ;

  // Dereference and addressof
  E(⟦ * ⟨Expression#E1⟩ ⟧)↓ft{:#fCopy}↓value(#Pr)↓vt{:#vCopy} → ⟦
    ⟨Instructions PointerExpr(#E1)↓ft{:#fCopy}↓value(#Pr)↓vt{:#vCopy}⟩
  ⟧↑used(#Pr) ;
  E(⟦ & ⟨Identifier#I1⟩ ⟧)↓value(#Pr) → ⟦
    MOV ⟨Reg#Pr⟩, ⟨Constant Reference(#I1)⟩
  ⟧↑used(#Pr) ;

  // Arithmetic Operators

  sort Instructions | scheme MulH(Instructions, Instructions, Reg) ;

  E(⟦ ⟨Expression#E1⟩ * ⟨Expression#E2⟩ ⟧)↓ft{:#fCopy}↓unused(MoRs(#R1, MoRs(#R2, #Rs)))
   ↓value(#Rv) ↓vt{:#v} → MulH(
    ⟦⟨Instructions E(#E1)↓value(#R1)↓ft{:#fCopy} ↓unused(#Rs) ↓vt{:#v}⟩⟧,
    ⟦⟨Instructions E(#E2)↓value(#R2)↓ft{:#fCopy} ↓unused(#Rs) ↓vt{:#v}⟩⟧,
    #Rv
  );

  MulH(⟦ ⟨Instructions#I1⟩ ⟧↑used(#R1), ⟦ ⟨Instructions#I2⟩ ⟧↑used(#R2), #Rv) → ⟦
    { ⟨Instructions#I1⟩ }
    { ⟨Instructions#I2⟩ }
    MUL ⟨Reg#Rv⟩, ⟨Reg#R1⟩, ⟨Reg#R2⟩
  ⟧↑used(#Rv) ;

  sort Instructions | scheme SubH(Instructions, Instructions, Reg) ;

  E(⟦ ⟨Expression#E1⟩ - ⟨Expression#E2⟩ ⟧)↓unused(MoRs(#R1, MoRs(#R2, #Rs)))↓ft{:#fCopy}
   ↓value(#Rv) ↓vt{:#v} → SubH(
    ⟦⟨Instructions E(#E1)↓value(#R1)↓ft{:#fCopy} ↓unused(#Rs) ↓vt{:#v}⟩⟧,
    ⟦⟨Instructions E(#E2)↓value(#R2)↓ft{:#fCopy} ↓unused(#Rs) ↓vt{:#v}⟩⟧,
    #Rv
  );

  SubH(⟦ ⟨Instructions#I1⟩ ⟧↑used(#R1), ⟦ ⟨Instructions#I2⟩ ⟧↑used(#R2), #Rv) → ⟦
    { ⟨Instructions#I1⟩ }
    { ⟨Instructions#I2⟩ }
    SUB ⟨Reg#Rv⟩, ⟨Reg#R1⟩, ⟨Reg#R2⟩
  ⟧↑used(#Rv) ;

  // Comparison Operators

  // Helpers:
  sort Instructions | scheme CompH(Instructions, Instructions, Instructions);
  CompH(⟦ ⟨Instructions#I1⟩ ⟧↑used(#R1), ⟦ ⟨Instructions#I2⟩ ⟧↑used(#R2), #I) → ⟦
    { ⟨Instructions#I1⟩ }
    { ⟨Instructions#I2⟩ }
    CMP ⟨Reg#R1⟩, ⟨Reg#R2⟩
    { ⟨Instructions#I⟩ }
  ⟧ ;
  sort Instructions | scheme CompH2(Instructions, Instructions);
  CompH2(⟦ ⟨Instructions#I1⟩ ⟧↑used(#R1), #I) → ⟦
    { ⟨Instructions#I1⟩ }
    CMP ⟨Reg#R1⟩, #0
    { ⟨Instructions#I⟩ }
  ⟧ ;

  sort Instructions | scheme GenComp(Expression) ↓ft ↓vt ↓return ↓unused ↓offset ↓true ↓false ↓value ;
  GenComp(⟦ ⟨Expression#E1⟩ < ⟨Expression#E2⟩ ⟧)↓ft{:#fCopy}↓vt{:#vCopy}↓true(#L) ↓false(#Lf) 
  ↓unused(MoRs(#R1, MoRs(#R2, #Rs))) → CompH(
    ⟦ ⟨Instructions E(#E1)↓ft{:#fCopy}↓vt{:#vCopy}↓value(#R1) ↓unused(MoRs(#R2, #Rs))⟩ ⟧, 
    ⟦ ⟨Instructions E(#E2)↓ft{:#fCopy}↓vt{:#vCopy}↓value(#R2) ↓unused(#Rs)⟩ ⟧, 
    ⟦
      BLT ⟨Label#L⟩
      B ⟨Label#Lf⟩
    ⟧
  ) ;
  GenComp(⟦ ⟨Expression#E1⟩ > ⟨Expression#E2⟩ ⟧)↓ft{:#fCopy}↓vt{:#vCopy}↓true(#L) ↓false(#Lf)
   ↓unused(MoRs(#R1, MoRs(#R2, #Rs))) → CompH(
    ⟦ ⟨Instructions E(#E1)↓ft{:#fCopy}↓vt{:#vCopy}↓value(#R1) ↓unused(MoRs(#R2, #Rs))⟩ ⟧, 
    ⟦ ⟨Instructions E(#E2)↓ft{:#fCopy}↓vt{:#vCopy}↓value(#R2) ↓unused(#Rs)⟩ ⟧, 
    ⟦
      BGT ⟨Label#L⟩
      B ⟨Label#Lf⟩
    ⟧
  ) ;
  GenComp(⟦ ⟨Expression#E1⟩ <= ⟨Expression#E2⟩ ⟧)↓ft{:#fCopy}↓vt{:#vCopy}↓true(#L) ↓false(#Lf)
   ↓unused(MoRs(#R1, MoRs(#R2, #Rs))) → CompH(
    ⟦ ⟨Instructions E(#E1)↓vt{:#vCopy}↓ft{:#fCopy}↓value(#R1) ↓unused(MoRs(#R2, #Rs))⟩ ⟧, 
    ⟦ ⟨Instructions E(#E2)↓vt{:#vCopy}↓ft{:#fCopy}↓value(#R2) ↓unused(#Rs)⟩ ⟧, 
    ⟦
      BLE ⟨Label#L⟩
      B ⟨Label#Lf⟩
    ⟧
  ) ;
  GenComp(⟦ ⟨Expression#E1⟩ >= ⟨Expression#E2⟩ ⟧)↓ft{:#fCopy}↓vt{:#vCopy}↓true(#L) ↓false(#Lf) 
  ↓unused(MoRs(#R1, MoRs(#R2, #Rs))) → CompH(
    ⟦ ⟨Instructions E(#E1)↓ft{:#fCopy}↓vt{:#vCopy}↓value(#R1) ↓unused(MoRs(#R2, #Rs))⟩ ⟧, 
    ⟦ ⟨Instructions E(#E2)↓ft{:#fCopy}↓vt{:#vCopy}↓value(#R2) ↓unused(#Rs)⟩ ⟧, 
    ⟦
      BGE ⟨Label#L⟩
      B ⟨Label#Lf⟩
    ⟧
  ) ;
  GenComp(⟦ ⟨Expression#E1⟩ == ⟨Expression#E2⟩ ⟧)↓ft{:#fCopy}↓vt{:#vCopy} ↓true(#L) ↓false(#Lf)
   ↓unused(MoRs(#R1, MoRs(#R2, #Rs))) → CompH(
    ⟦ ⟨Instructions E(#E1)↓ft{:#fCopy}↓vt{:#vCopy}↓value(#R1) ↓unused(MoRs(#R2, #Rs))⟩ ⟧, 
    ⟦ ⟨Instructions E(#E2)↓ft{:#fCopy}↓vt{:#vCopy}↓value(#R2) ↓unused(#Rs)⟩ ⟧, 
    ⟦
      BEQ ⟨Label#L⟩
      B ⟨Label#Lf⟩
    ⟧
  ) ;
  GenComp(⟦ ⟨Expression#E1⟩ != ⟨Expression#E2⟩ ⟧)↓ft{:#fCopy}↓vt{:#vCopy}↓true(#L) ↓false(#Lf)
   ↓unused(MoRs(#R1, MoRs(#R2, #Rs))) → CompH(
    ⟦ ⟨Instructions E(#E1)↓ft{:#fCopy}↓vt{:#vCopy}↓value(#R1) ↓unused(MoRs(#R2, #Rs))⟩ ⟧, 
    ⟦ ⟨Instructions E(#E2)↓ft{:#fCopy}↓vt{:#vCopy}↓value(#R2) ↓unused(#Rs)⟩ ⟧, 
    ⟦
      BNE ⟨Label#L⟩
      B ⟨Label#Lf⟩
    ⟧
  ) ;
  GenComp(⟦ ⟨Expression#E1⟩ ⟧)↓ft{:#fCopy}↓vt{:#vCopy}↓true(#L) ↓false(#Lf)
   ↓unused(MoRs(#R1, MoRs(#R2, #Rs))) → CompH2(
    ⟦ ⟨Instructions E(#E1)↓ft{:#fCopy}↓vt{:#vCopy}↓value(#R1) ↓unused(#Rs)⟩ ⟧,  
    ⟦
      BNE ⟨Label#L⟩
      B ⟨Label#Lf⟩
    ⟧
  ) ;

  // Logical Operators

  // Helpers:
  sort Instructions | scheme OrH(Instructions, Instructions);
  sort Instructions | scheme AndH(Instructions, Instructions);

  OrH(⟦ ⟨Instructions#I1⟩ ⟧↑used(#R1), ⟦ ⟨Instructions#I2⟩ ⟧↑used(#R2)) → ⟦
    { ⟨Instructions#I1⟩ }
    { ⟨Instructions#I2⟩ }
    ORR ⟨Reg#R1⟩, ⟨Reg#R1⟩, ⟨Reg#R2⟩
  ⟧ ;
  AndH(⟦ ⟨Instructions#I1⟩ ⟧↑used(#R1), ⟦ ⟨Instructions#I2⟩ ⟧↑used(#R2)) → ⟦
    { ⟨Instructions#I1⟩ }
    { ⟨Instructions#I2⟩ }
    AND ⟨Reg#R1⟩, ⟨Reg#R1⟩, ⟨Reg#R2⟩
  ⟧ ;

  E(⟦ ⟨Expression#E1⟩ && ⟨Expression#E2⟩ ⟧)↓ft{:#fCopy}↓unused(MoRs(#R1, MoRs(#R2, #Rs))) → AndH(
    ⟦ ⟨Instructions E(#E1)↓ft{:#fCopy}↓value(#R1) ↓unused(MoRs(#R2, #Rs))⟩ ⟧,
    ⟦ ⟨Instructions E(#E2)↓ft{:#fCopy}↓value(#R2) ↓unused(#Rs)⟩ ⟧
  ) ;
  E(⟦ ⟨Expression#E1⟩ || ⟨Expression#E2⟩ ⟧)↓ft{:#fCopy}↓unused(MoRs(#R1, MoRs(#R2, #Rs))) → OrH(
    ⟦ ⟨Instructions E(#E1)↓ft{:#fCopy}↓value(#R1) ↓unused(MoRs(#R2, #Rs))⟩ ⟧,
    ⟦ ⟨Instructions E(#E2)↓ft{:#fCopy}↓value(#R2) ↓unused(#Rs)⟩ ⟧
  ) ;

}
