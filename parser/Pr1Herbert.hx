module compilers.Pr1Herbert {

    // Special characters for HACS (so I can copy and paste them): ⟨, ⟩, ¬, ¶, ↑, →, ↓, ⟦, ⟧)
    
    // Keyword types
    // These are defined up here so they won't be caught as tokens
    token Int | "int" ;
    token Char | "char" ;
    token If | "if" ;
    token Else | "else" ;
    token While | "while" ;
    token Ret | "return" ; 
    token Func | "function" ;
    token Null | "null" ;
    token Size | "sizeof" ;
    token Var | "var" ;

    // Identifier
    token Id | ⟨Alpha⟩(⟨Alpha⟩|[0-9])*;

    // Integer
    token Integer | [0-9]+ ;

    // String
    // Consists of zero or more of either:
    // 1. non-backslash and non-newline characters
    // 2. a backslash followed by an escape character / sequence
    // surrounded by double quotes
    token Str | \"([^\"\n]|\\(⟨Newline⟩|⟨Tab⟩|\\|\"|⟨Octal⟩|⟨Hex⟩))*\" ;

    // Comments
    // Single line comments consist of a double-slash followed by any number of non-newline characters
    // Multi-line comments consist of zero or more of either:
    // 1. non-asterisk and non-slash characters
    // 2. an asterisk followed by a non-slash character
    // 3. a slash followed by a non-asterish character
    // surrounded by a "/*" and "*/"
    space [ \t\f\r\n] | "//" .* | "/*" ("/"+[^*]|"*"+[^/]|[^*/]|\n)* "*/" ;

    // Fragments
    token fragment Newline | \n ;
    token fragment Tab | \t ;
    token fragment Alpha | [A-Za-z_$] ;
    token fragment Octal | [0-7][0-7]?[0-7]? ;
    token fragment Hex | x[0-9A-Fa-f][0-9A-Fa-f] ;

    // Expressions
    sort Expr   | ⟦ ⟨Expr@2⟩||⟨Expr@1⟩ ⟧@1
                | ⟦ ⟨Expr@3⟩&&⟨Expr@2⟩ ⟧@2
                | ⟦ ⟨Expr@3⟩==⟨Expr@4⟩ ⟧@3 
                | ⟦ ⟨Expr@3⟩!=⟨Expr@4⟩ ⟧@3
                | ⟦ ⟨Expr@4⟩<⟨Expr@5⟩ ⟧@4 
                | ⟦ ⟨Expr@4⟩>⟨Expr@5⟩ ⟧@4 
                | ⟦ ⟨Expr@4⟩<=⟨Expr@5⟩ ⟧@4 
                | ⟦ ⟨Expr@4⟩>=⟨Expr@5⟩ ⟧@4
                | ⟦ ⟨Expr@5⟩+⟨Expr@6⟩ ⟧@5 
                | ⟦ ⟨Expr@5⟩-⟨Expr@6⟩ ⟧@5             
                | ⟦ ⟨Expr@6⟩*⟨Expr@7⟩ ⟧@6 
                | ⟦ ⟨Expr@6⟩/⟨Expr@7⟩ ⟧@6 
                | ⟦ ⟨Expr@6⟩%⟨Expr@7⟩ ⟧@6                
                | ⟦ !⟨Expr@7⟩ ⟧@7 
                | ⟦ -⟨Expr@7⟩ ⟧@7 
                | ⟦ +⟨Expr@7⟩ ⟧@7 
                | ⟦ *⟨Expr@7⟩ ⟧@7 
                | ⟦ &⟨Expr@7⟩ ⟧@7                
                | ⟦ ⟨Expr@8⟩(⟨Exprs⟩) ⟧@8
                | ⟦ ⟨Null⟩(⟨Type⟩) ⟧@8
                | ⟦ ⟨Size⟩(⟨Type⟩) ⟧@8
                | ⟦ ⟨Id⟩ ⟧@9
                | ⟦ ⟨Str⟩ ⟧@9
                | ⟦ ⟨Integer⟩ ⟧@9
                | sugar ⟦ (⟨Expr#⟩) ⟧@9→Expr# ;
    
    // L-value expression
    sort LExpr  | ⟦ *⟨Expr⟩ ⟧ | ⟦ ⟨Id⟩ ⟧ ;
    
    // For an arbitrary number of expressions, used for: Expr(Expr, . . ., Expr)
    sort Exprs  | ⟦ ⟧ | ⟦ ⟨Expr⟩, ⟨Exprs⟩ ⟧ | ⟦ ⟨Expr⟩ ⟧ ;

    // Types
    sort Type   | ⟦ *⟨Type@2⟩ ⟧@1
                | ⟦ ⟨Type@3⟩(⟨Types⟩) ⟧@2
                | ⟦ ⟨Int⟩ ⟧@3
                | ⟦ ⟨Char⟩ ⟧@3
                | sugar ⟦ (⟨Type#⟩) ⟧@3→Type# ;
    
    // For an arbitrary number of type, used for: Type(Type, . . ., Type)
    sort Types  | ⟦ ⟨Type⟩, ⟨Types⟩ ⟧ | ⟦ ⟧ | ⟦ ⟨Type⟩ ⟧;

    // Statements
    sort Stmt   | ⟦ ⟨Var⟩ ⟨Type⟩ ⟨Id⟩ ; ⟧
                | ⟦ ⟨LExpr⟩ = ⟨Expr⟩ ; ⟧
                | ⟦ ⟨If⟩ (⟨Expr⟩) ⟨Stmt⟩ ⟧
                | ⟦ ⟨If⟩ (⟨Expr⟩) ⟨Stmt⟩ ⟨Else⟩ ⟨Stmt⟩ ⟧
                | ⟦ ⟨While⟩ (⟨Expr⟩) ⟨Stmt⟩ ⟧
                | ⟦ ⟨Ret⟩ ⟨Expr⟩ ; ⟧
                | ⟦ { ⟨Stmts⟩ } ⟧ ; 
    sort Stmts  | ⟦ ⟨Stmt⟩ ⟨Stmts⟩ ⟧ | ⟦ ⟧ ;

    // Declarations
    sort Decl     | ⟦ ⟨Func⟩ ⟨Type⟩ main (⟨Params⟩) { ⟨Stmts⟩ } ⟨DeclEnd⟩ ⟧
                  | ⟦ ⟨Func⟩ ⟨Type⟩ ⟨Id⟩ (⟨Params⟩) { ⟨Stmts⟩ } ⟨Decl⟩ ⟧ ;
    sort DeclEnd  | ⟦ ⟨Func⟩ ⟨Type⟩ main (⟨Params⟩) { ⟨Stmts⟩ } ⟨DeclEnd⟩ ⟧
                  | ⟦ ⟨Func⟩ ⟨Type⟩ ⟨Id⟩ (⟨Params⟩) { ⟨Stmts⟩ } ⟨DeclEnd⟩ ⟧
                  | ⟦⟧ ;
      
    // Sorts for arbitrarily-long parameter lists
    sort Param  | ⟦ ⟨Type⟩ ⟨Id⟩ ⟧ ;
    sort Params | ⟦ ⟨Param⟩, ⟨Params⟩ ⟧ | ⟦ ⟨Param⟩ ⟧ | ⟦⟧ ; 

    // Program
    main sort Program | ⟦ ⟨Decl⟩ ⟧ ;

}
