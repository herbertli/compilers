module compilers.Part3Herbert {

    // Syntax.
    token INT | [0-9]+;
    sort List | ⟦⟨INT⟩ ⟨List⟩⟧ | ⟦⟧;
    sort Bool | ⟦TRUE⟧ | ⟦FALSE⟧;

    sort Computed | scheme Add(Computed, Computed);
    Add(#1, #2) → ⟦#1 + #2⟧;

    sort Computed | scheme LenS(List);
    LenS(⟦⟧) → ⟦0⟧;
    LenS(⟦⟨INT#1⟩ ⟨List#2⟩⟧) → Add(⟦1⟧, LenS(#2));

    attribute ↑len(Computed);
    sort List | ↑len;
    ⟦ ⟧↑len(⟦0⟧);
    ⟦⟨INT#1⟩ ⟨List#2↑len(#n)⟩⟧↑len(Add(#n, ⟦1⟧));

    sort Computed | scheme LenA(List);
    LenA(#x ↑len(#n)) →#n;

    sort List | scheme Append(INT, List);
    Append(#1, ⟦⟧) → ⟦⟨INT#1⟩⟧;
    Append(#1, ⟦⟨INT#2⟩ ⟨List#3⟩⟧) → ⟦⟨INT#2⟩ ⟨List Append(#1, #3)⟩⟧;

    sort List | scheme AppendThree(List);
    AppendThree(#) → Append(⟦3⟧, #);

    sort List | scheme Rev(List);
    Rev(⟦⟨List#1⟩⟧) → ReverseH(⟦⟨List#1⟩⟧, ⟦⟧);

    sort List | scheme ReverseH(List, List);
    ReverseH(⟦⟧, #1) → #1;
    ReverseH(⟦⟨INT#1⟩ ⟨List#2⟩⟧, ⟦⟨List#3⟩⟧) → ReverseH(⟦⟨List#2⟩⟧, ⟦⟨INT#1⟩ ⟨List#3⟩⟧);

    sort Bool | scheme Match(List, List);
    Match(⟦⟧, ⟦⟧) → ⟦TRUE⟧;
    Match(⟦⟨INT#1⟩ ⟨List#2⟩⟧, ⟦⟧) → ⟦FALSE⟧;
    Match(⟦⟧, ⟦⟨INT#1⟩ ⟨List#2⟩⟧) → ⟦FALSE⟧;
    Match(⟦⟨INT#1⟩ ⟨List#2⟩⟧, ⟦⟨INT#1⟩ ⟨List#3⟩⟧) → Match(⟦⟨List#2⟩⟧, ⟦⟨List#3⟩⟧);
    Match(⟦⟨INT#1⟩ ⟨List#3⟩⟧, ⟦⟨INT#2⟩ ⟨List#4⟩⟧) → ⟦FALSE⟧;

    sort Bool | scheme MatchEmpty(List);
    MatchEmpty(⟦⟨List#1⟩⟧) → Match(⟦⟧, #1);

    sort Bool | scheme PalS(List);
    PalS(⟦⟨List#1⟩⟧) → Match(Rev(⟦⟨List#1⟩⟧), ⟦⟨List#1⟩⟧);

    sort Bool | scheme PalA(List);
    PalA(#x ↑rev(#n)) → Match(#x, #n);

    attribute ↑rev(List);
    sort List | ↑rev;
    ⟦⟧↑rev(⟦⟧);
    ⟦⟨INT#1⟩ ⟨List#2↑rev(#n)⟩⟧↑rev(Append(#1, #n));

}
