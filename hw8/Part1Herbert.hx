module compilers.Part1Herbert {

    // Syntax.
    token INT | [0-9]+;
    sort List | ⟦⟨INT⟩ ⟨List⟩⟧ | ⟦⟧;

    sort Computed | scheme Add(Computed, Computed);
    Add(#1, #2) → ⟦#1 + #2⟧;

    sort Computed | scheme LenS(List);
    LenS(⟦⟧) → ⟦0⟧;
    LenS(⟦⟨INT#1⟩ ⟨List#2⟩⟧) → Add(⟦1⟧, LenS(#2));

    attribute ↑len(Computed);
    sort List | ↑len;
    ⟦ ⟧↑len(⟦0⟧);
    ⟦⟨INT#1⟩ ⟨List#2↑len(#n)⟩⟧↑len(Add(#n, ⟦1⟧));

    sort Computed | scheme LenA(List);
    LenA(#x ↑len(#n)) →#n;

}
